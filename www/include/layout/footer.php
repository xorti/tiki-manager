		<footer>
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<span class="navbar-text mx-auto">
						<?php echo TITLE; ?>
					</span>
				</div>
			</nav>
		</footer>

		<script src="/vendor/components/jquery/jquery.min.js"></script>
		<script src="/vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/themes/trim.js"></script>
	</body>
</html>
